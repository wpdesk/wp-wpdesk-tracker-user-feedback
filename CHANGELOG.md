## [1.0.2] - 2024-03-12
### Fixed
- permissions check in AJAX action

## [1.0.1] - 2020-08-19
### Fixed
- changed deprecated jQuery .load() to .on('load', handler) 

## [1.0.0] - 2020-06-30
### Added
- First version
