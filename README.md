[![pipeline status](https://gitlab.com/wpdesk/wp-wpdesk-tracker-user-feedback/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-wpdesk-tracker-user-feedback/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-wpdesk-tracker-user-feedback/badges/master/coverage.svg)](https://gitlab.com/wpdesk/wp-wpdesk-tracker-user-feedback/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-wpdesk-tracker-user-feedback/v/stable)](https://packagist.org/packages/wpdesk/wp-wpdesk-tracker-user-feedback) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-wpdesk-tracker-user-feedback/downloads)](https://packagist.org/packages/wpdesk/wp-wpdesk-tracker-user-feedback) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-wpdesk-tracker-user-feedback/v/unstable)](https://packagist.org/packages/wpdesk/wp-wpdesk-tracker-user-feedback) 
[![License](https://poser.pugx.org/wpdesk/wp-wpdesk-tracker-user-feedback/license)](https://packagist.org/packages/wpdesk/wp-wpdesk-tracker-user-feedback)

#WordPress Library to track user feedback data.

This library can track user feedaback and send it to WPDesk server.
