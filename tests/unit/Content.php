<?php

use WP_Mock\Tools\TestCase;
use WPDesk\Tracker\UserFeedback\AjaxUserFeedbackDataHandler;
use WPDesk\Tracker\UserFeedback\PluginData;
use WPDesk\Tracker\UserFeedback\Scripts;

class Content extends TestCase {

	const THICKBOX_ID = 'thickbox-title';
	const HOOK_SUFFIX = 'hook_suffix';
	const THICKBOX_QUESTION = 'thickbox question';
	const THICKBOX_HEADING = 'thickbox heading';
	protected $view = __DIR__ . '/../../src/WPDesk/Tracker/UserFeedback/views/abstract.php';

	public function setUp(): void {
		\WP_Mock::setUp();
	}

	public function tearDown(): void {
		\WP_Mock::tearDown();
	}

	/**
	 * @return \WPDesk\Tracker\UserFeedback\UserFeedbackData
	 */
	protected function prepareUserData() {
		$user_data = new \WPDesk\Tracker\UserFeedback\UserFeedbackData( self::THICKBOX_ID, self::THICKBOX_ID, self::THICKBOX_HEADING, self::THICKBOX_QUESTION, self::HOOK_SUFFIX );
		$user_data->add_feedback_option( new \WPDesk\Tracker\UserFeedback\UserFeedbackOption( 'option1', 'option 1', false ) );
		$user_data->add_feedback_option( new \WPDesk\Tracker\UserFeedback\UserFeedbackOption( 'option2', 'option 2', true, 'placeholder' ) );
		return $user_data;
	}

	/**
	 * Get content.
	 *
	 * @return string
	 */
	protected function getContent() {

		\WP_Mock::userFunction( 'wp_create_nonce', array(
			'return' => 'nonce',
		) );

		\WP_Mock::userFunction( 'admin_url', array(
			'return' => 'admin_url',
		) );

		$thickbox_id = self::THICKBOX_ID;
		$thickbox_title = self::THICKBOX_ID;
		$ajax_action = '';
		$ajax_nonce = '';
		$button_send_text = 'Proceed';
		$thickbox_heading = self::THICKBOX_HEADING;
		$thickbox_question = self::THICKBOX_QUESTION;
		$thickbox_feedback_options = $this->prepareUserData()->get_feedback_options();
		$thickbox_all_options = count( $thickbox_feedback_options );
		$ajax_action = AjaxUserFeedbackDataHandler::AJAX_ACTION . self::THICKBOX_ID;
		$ajax_nonce = wp_create_nonce( $ajax_action );
		ob_start();
		include $this->view;
		return ob_get_clean();
	}

}
