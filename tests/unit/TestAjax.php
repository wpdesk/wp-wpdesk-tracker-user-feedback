<?php

use WP_Mock\Tools\TestCase;
use WPDesk\Tracker\UserFeedback\AjaxUserFeedbackDataHandler;
use WPDesk\Tracker\UserFeedback\PluginData;

class TestAjax extends Content {

	public function setUp(): void {
		\WP_Mock::setUp();
	}

	public function tearDown(): void {
		\WP_Mock::tearDown();
	}

	public function testHandleAjaxRequest() {
		\WP_Mock::userFunction( 'check_ajax_referer', array(
			'return' => true,
		) );
		\WP_Mock::userFunction( 'current_user_can', array(
		    'return' => true,
		) );

		$_REQUEST['reason'] = 'other';
		$_REQUEST['additional_info'] = 'test';

		$sender = $this->createMock(\WPDesk_Tracker_Sender::class);

		$ajax = new AjaxUserFeedbackDataHandler($this->prepareUserData(), $sender);
		$this->expectOutputString('');
		$ajax->handle_ajax_request();
	}
}
