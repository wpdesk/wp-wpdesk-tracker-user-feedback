<?php

use WPDesk\Tracker\UserFeedback\PluginData;
use WPDesk\Tracker\UserFeedback\Scripts;

class TestScripts extends Content {

	protected $view = __DIR__ . '/../../src/WPDesk/Tracker/UserFeedback/views/scripts.php';

	/**
	 * Test getContent.
	 */
	public function testGetContent() {
		$scripts = new Scripts($this->prepareUserData());
		$this->assertEquals($this->getContent(), $scripts->get_content());
	}

}