<?php

use WPDesk\Tracker\UserFeedback\PluginData;
use WPDesk\Tracker\UserFeedback\Thickbox;

class TestThickbox extends Content {

	protected $view = __DIR__ . '/../../src/WPDesk/Tracker/UserFeedback/views/thickbox.php';

	/**
	 * Test getContent.
	 */
	public function testGetContent() {
		$thickbox = new Thickbox($this->prepareUserData());
		$this->assertEquals( $this->getContent(), $thickbox->get_content() );
	}

}