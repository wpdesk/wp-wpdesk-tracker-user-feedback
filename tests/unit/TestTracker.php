<?php

use WP_Mock\Tools\TestCase;
use WPDesk\Tracker\UserFeedback\AjaxUserFeedbackDataHandler;
use WPDesk\Tracker\UserFeedback\PluginData;
use WPDesk\Tracker\UserFeedback\Scripts;
use WPDesk\Tracker\UserFeedback\Thickbox;
use WPDesk\Tracker\UserFeedback\Tracker;
use WPDesk\Tracker\UserFeedback\TrackerFactory;

class TestTracker extends Content {

	const PLUGIN_SLUG = 'slug';
	const PLUGIN_FILE = 'file';
	const PLUGIN_TITLE = 'title';

	public function setUp(): void {
		\WP_Mock::setUp();
	}

	public function tearDown(): void {
		\WP_Mock::tearDown();
	}

	/**
	 * Test hooks.
	 */
	public function testHooks() {
		$sender = $this->createMock(\WPDesk_Tracker_Sender::class);
		$user_feedback_data = $this->prepareUserData();
		$userFeedbackTracker = new Tracker(
			$user_feedback_data,
			new Scripts($user_feedback_data),
			new Thickbox($user_feedback_data),
			new AjaxUserFeedbackDataHandler($user_feedback_data, $sender )
		);

		\WP_Mock::expectActionAdded( 'admin_print_footer_scripts-' . self::HOOK_SUFFIX, [ $userFeedbackTracker,
			'print_user_feedback_scripts'
		]);
		\WP_Mock::expectActionAdded( 'admin_footer-' . self::HOOK_SUFFIX, [ $userFeedbackTracker,
			'print_user_feedback_thickbox'
		]);

		$userFeedbackTracker->hooks();

		$this->assertTrue(true);
	}

	/**
	 * Tests if there is script in output.
	 */
	public function testPrintUserFeedbackScripts() {
		$user_feedback_data = $this->prepareUserData();
		$tracker = TrackerFactory::create_custom_tracker($user_feedback_data);
		$this->expectOutputRegex('/script/i');
		$tracker->print_user_feedback_scripts();
		$this->assertTrue(true);
	}

	/**
	 * Tests if there is div in output.
	 */
	public function testPrintUserFeedbackThickbox() {
		$user_feedback_data = $this->prepareUserData();
		$tracker = TrackerFactory::create_custom_tracker($user_feedback_data);
		$this->expectOutputRegex('/div/i');
		$tracker->print_user_feedback_thickbox();
		$this->assertTrue(true);
	}

}
