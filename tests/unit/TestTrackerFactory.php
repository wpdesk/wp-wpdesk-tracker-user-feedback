<?php

use WP_Mock\Tools\TestCase;

class TestTrackerFactory extends Content {

	public function setUp(): void {
		\WP_Mock::setUp();
	}

	public function tearDown(): void {
		\WP_Mock::tearDown();
	}

	/**
	 * Test createCustomTracker.
	 */
	public function testCreateCustomTracker() {
		$sender = $this->createMock(\WPDesk_Tracker_Sender::class);
		$user_feedback_data = $this->prepareUserData();
		$tracker = \WPDesk\Tracker\UserFeedback\TrackerFactory::create_custom_tracker($user_feedback_data);
		$this->assertInstanceOf( \WPDesk\Tracker\UserFeedback\Tracker::class, $tracker);
	}

}
